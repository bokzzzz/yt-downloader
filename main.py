import re
from pytube import Playlist
import os
from moviepy.editor import *


def downloadAudio(videoTmp):
    try:
        print('Skida se: {}'.format(videoTmp.title))
        mp4File = videoTmp.streams.get_lowest_resolution().download(output_path=DOWNLOAD_DIR)
        base, ext = os.path.splitext(mp4File)
        videoMp4 = VideoFileClip(os.path.join(mp4File))
        videoMp4.audio.write_audiofile(os.path.join(base + ".mp3"))
        videoMp4.close()
        os.remove(mp4File)
    except:
        print("Postoji numera!")


playlistLink = input("Unesite link plejliste: ")
playlist = Playlist(playlistLink)
DOWNLOAD_DIR = input("Unesite putanju do direktrijuma: ")
playlist._video_regex = re.compile(r"\"url\":\"(/watch\?v=[\w-]*)")
print(f"Duzina plejliste je: {len((playlist.video_urls))}")
listThread = []
for video in playlist.videos:
    downloadAudio(video)
